import {Button, Input} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import __EVENT from './filter.__.fn.on.event';
import __Zn from "zs/variant-column-qr/zero.uca.dependency";
import Addon from "zs/variant-column-qr/filter.__.fn.sift.addon";

const CSS_DROPDOWN = ({
    padding: 8,
})
const CSS_INPUT = ({
    width: 188,
    marginBottom: 8,
    display: "block"
});
const CSS_YES = ({
    width: 90,
    marginRight: 8,
});
const CSS_NO = ({
    width: 90,
})

const FilterTextBox = (props) => {
    const [searchText, setSearchText] = useState(props.value);
    useEffect(() => {
        setSearchText(props.value);
    }, [props.value]);
    const {
        config = {},
        column,
        configuration,
        reference,
    } = props;
    const {
        placeholder = "",
        button = {}
    } = config;
    const attrEvent = {
        ...configuration,
        field: column.dataIndex,
        value: props.value,
        // Effect
        setSearchText,
    };

    const attrInput = {};
    attrInput.placeholder = placeholder;
    attrInput.style = CSS_INPUT;
    attrInput.onChange = __EVENT.onChangeFn(attrEvent);
    attrInput.onPressEnter = __EVENT.onConfirmFn(attrEvent, reference);
    attrInput.value = searchText;

    const attrYes = {};
    attrYes.type = "primary";
    attrYes.icon = (<SearchOutlined/>);
    attrYes.style = CSS_YES;
    attrYes.danger = true;
    attrYes.onClick = __EVENT.onConfirmFn(attrEvent, reference);

    const attrNo = {};
    attrNo.style = CSS_NO;
    attrNo.disabled = !searchText;
    attrNo.onClick = __EVENT.onResetFn(attrEvent);
    return (
        <div style={CSS_DROPDOWN}>
            <Input {...attrInput}/>
            <Button {...attrYes}>
                {button.search ? button.search : false}
            </Button>
            <Button {...attrNo}>
                {button.reset ? button.reset : false}
            </Button>
            {__Zn.anchorColumn(column.dataIndex, Addon.siftClean(attrEvent))}
        </div>
    )
}

// eslint-disable-next-line import/no-anonymous-default-export
export default (column, config = {}, reference) => (configuration) => {
    const attrs = {
        column,
        config,
        reference,
        configuration,
    }
    const {$condition = {}} = reference.state;
    let value = $condition[column.dataIndex];
    return (
        <FilterTextBox {...attrs} value={value}/>
    )
}