import __Zn from './zero.form.dependency';
import __Ft from './form.__.fn.run.common';

export default (reference, config = {}, redux = false) => (event) => {
    __Zn.prevent(event);

    return __Ft.runSubmit(reference, redux).catch(error => {


        return __Zn.ajaxError(reference, error);
    })
}
