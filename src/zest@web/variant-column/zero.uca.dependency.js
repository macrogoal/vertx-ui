import __Zn from '../zero.module.dependency';

import WEB_FILTER from '../variant-column-qr';

export default {
    ...__Zn,
    WEB_FILTER,
}