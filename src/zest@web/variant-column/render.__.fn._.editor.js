import __Zn from './zero.uca.dependency';
import __RFT from '../form.equip.__.fn.raft.04.render';

export default {
    EDITOR: (reference, column = {}) => {
        const cell = {};                                                    // Cell 处理
        cell.field = column.dataIndex;
        const $cell = column.config ? column.config : {};                   // 旧版优先级高
        const $config = column.$config ? column.$config : {};               // 新版优先级低
        Object.assign(cell, $config, $cell);
        if (!cell.optionJsx) {
            cell.optionJsx = {};
        }
        if (!cell.optionConfig) {
            cell.optionConfig = {};
        }
        return (text, record, index) => {
            const {disabled = false, config = {}} = reference.props;
            /* 处理专用 */
            const rowCell = __Zn.clone(cell);
            rowCell.optionJsx.value = record[rowCell.field];
            rowCell.optionJsx.disabled = disabled;                          // 禁用处理
            rowCell.column = {
                index,                                                      // 列变更时的索引
                format: config.format,                                      // 格式信息
            }
            const render = __RFT.raftRender(rowCell, {
                addOn: {
                    reference
                },
            });
            return render(record);
        }
    },
}