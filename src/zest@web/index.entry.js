import func_ask_ai_form_external from './ask.fn.ai.form.external';
import func_ask_ai_form_external_uca from './ask.fn.ai.form.external.uca';

import func_aureole_v4_child from './aureole.fn.ai.child.v4.patch';

import func_autonomy_ai_action from './autonomy.fn.ai.action';
import func_autonomy_ai_child from './autonomy.fn.ai.child';

import func_automatic_ai_control_menu from './automatic.fn.ai.control.menu';
import func_automatic_ai_control_tab from './automatic.fn.ai.control.tab';
import func_automatic_ai_control_tree from './automatic.fn.ai.control.tree';

import func_autonomy_ai_error from './autonomy.fn.ai.error';

import func_autonomy_ai_unit_buttons from './autonomy.fn.ai.unit.buttons';
import func_autonomy_ai_unit_element from './autonomy.fn.ai.unit.element';
import func_autonomy_ai_unit_link from './autonomy.fn.ai.unit.link';

import func_autonomy_ai_view from './autonomy.fn.ai.view';
import func_autonomy_ui_cloud_element from './autonomy.fn.ui.cloud.element';

import func_behavior_rx_checked from './behavior.fn.rx.checked';
import func_behavior_rx_workflow from './behavior.fn.rx.workflow';

import func_chart_g2_util_lifecycle from './chart.fn.g2.util.lifecycle';
import func_chart_g2_viewer_graph from './chart.fn.g2.viewer.graph';

import func_drag_drop_dnd_operation from './drag.drop.fn.dnd.operation';

import func_equip_cab_cap_dual from './equip.fn.cab.cap.dual';
import func_equip_config_container from './equip.fn.config.container';
import func_equip_config_element from './equip.fn.config.element';
import func_equip_plugin_extension from './equip.fn.plugin.extension';
import func_equip_width_calculate from './equip.fn.width.calculate';

import func_form_equip_raft_phase from './form.equip.fn._.raft.phase';
import func_form_toolkit from './form.fn._.toolkit';
import func_form_connect_trigger from './form.fn.connect.trigger';
import func_form_submit_data_io from './form.submit.fn.data.io';
import func_form_submit_form_antd4 from './form.submit.fn.form.antd4';

import func_statistic_aggr_elementary from './statistic.fn.aggr.elementary';

import func_table_config_table from './table.fn.config.table';
import func_table_config_executor from './table.fn.config.executor';
import func_table_variant_column from './variant-column';

import func_workflow_value_exchange from './workflow.fn.value.exchange';
// 将来会拿掉
import remove_func_in_future from './index.remove.future';
import V_UCA_CONTAINER from './variant-uca-container';
import V_UCA_CONTROL from './variant-uca';

export default {
    V_UCA_CONTAINER,
    V_UCA_CONTROL,

    ...func_ask_ai_form_external,
    ...func_ask_ai_form_external_uca,

    ...func_aureole_v4_child,

    ...func_automatic_ai_control_menu,
    ...func_automatic_ai_control_tab,
    ...func_automatic_ai_control_tree,

    ...func_autonomy_ai_action,
    ...func_autonomy_ai_child,

    ...func_autonomy_ai_error,
    ...func_autonomy_ai_unit_buttons,
    ...func_autonomy_ai_unit_element,
    ...func_autonomy_ai_unit_link,

    ...func_autonomy_ai_view,
    ...func_autonomy_ui_cloud_element,

    ...func_behavior_rx_checked,
    ...func_behavior_rx_workflow,

    ...func_chart_g2_util_lifecycle,
    ...func_chart_g2_viewer_graph,

    ...func_drag_drop_dnd_operation,

    ...func_equip_cab_cap_dual,
    ...func_equip_config_container,
    ...func_equip_config_element,
    ...func_equip_plugin_extension,
    ...func_equip_width_calculate,

    ...func_form_equip_raft_phase,
    ...func_form_connect_trigger,
    ...func_form_toolkit,
    ...func_form_submit_data_io,
    ...func_form_submit_form_antd4,

    ...func_statistic_aggr_elementary,

    ...func_table_config_table,
    ...func_table_config_executor,
    ...func_table_variant_column,

    ...func_workflow_value_exchange,

    ...remove_func_in_future
}