import {Tabs} from "antd";
import React from "react";
import __Zn from './zero.module.dependency';
import __V4 from './aureole.fn.ai.child.v4.patch';

const aiTabPage = (reference, children = {}) => {
    const {$tabs = {}} = reference.state ?
        reference.state : {};
    const {items = [], ...rest} = $tabs;
    // v4
    const $items = __V4.v4Items(items, {
        // itemFn 默认
        childFn: (item) => {
            const fnRender = children[item.key];
            return __Zn.isFunction(fnRender) ? fnRender(item) :
                `对不起，children 函数丢失：${item.key}`
        }
    }, reference);
    /*
            {items.map(item => {
                const fnRender = children[item.key];
                return (
                    <Tabs.?abPane {...item}>
                        {__Zn.isFunction(fnRender) ? fnRender(item) :
                            `对不起，children 函数丢失：${item.key}`}
                    </Tabs.?abPane>
                )
            })}
     */
    return (
        <Tabs {...rest} items={$items}/>
    )
}
export default {
    aiTabPage,
}