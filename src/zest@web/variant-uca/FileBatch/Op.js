import __Zn from '../zero.uca.dependency';

const rxVCard = (reference) => (event) => {
    __Zn.prevent(event);
    __Zn.of(reference).in({
        $listType: __Zn.Env.TYPE_UPLOAD.CARD,
    }).done();
    // reference.?etState({$listType: "picture-card"});
}
const rxVList = (reference) => (event) => {
    __Zn.prevent(event);
    __Zn.of(reference).in({
        $listType: __Zn.Env.TYPE_UPLOAD.TEXT,
    }).done();
}
const rxVClean = (reference) => (event) => {
    __Zn.prevent(event);
    __Zn.of(reference).in({
        fileList: []
    }).handle(() => {

        __Zn.fn(reference).onChange([]);
    })
    // __Zn.fn(reference).onChange([]);
    // reference.?etState({fileList: []});
}
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    rxVCard,
    rxVList,
    rxVClean,
}