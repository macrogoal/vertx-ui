import React from 'react';
import {Button, Col, Row, Tag, Tooltip, Upload} from 'antd';
import Op from './Op';
import __Zn from '../zero.uca.dependency';

const renderFile = (reference) => {
    const action = __Zn.fromHoc(reference, "action");
    // 按钮操作
    const button = action.button ? action.button : {};
    const {handler = {}, fileList = [], $listType} = reference.state;
    const attrs = {};
    attrs.fileList = __Zn.xtUploadMime(fileList);
    attrs.showUploadList = {
        showPreviewIcon: true,          // 预览专用
        showRemoveIcon: true
    };
    attrs.multiple = true;              // 是否支持多文件上传
    attrs.listType = $listType;          // 文件卡片显示
    const upload = {
        ...handler,
        ...attrs
    };
    return (
        <Upload.Dragger {...upload}
                        className={"operation"}>
            {__Zn.v4Icon(button.icon, {className: "op-icon"})}
            <label className={"op-label"}>{button.text}</label>
        </Upload.Dragger>
    );
}
const renderBar = (reference) => {
    const info = __Zn.fromHoc(reference, "info");
    const {$listType, fileList = []} = reference.state;

    const btnCard = {}
    // v4
    btnCard.icon = __Zn.v4Icon("appstore");
    btnCard.type = __Zn.Env.TYPE_UPLOAD.CARD === $listType
        ? "primary" : "default";
    btnCard.onClick = Op.rxVCard(reference);

    const btnList = {};
    // v4
    btnList.icon = __Zn.v4Icon("unordered-list");
    btnList.type = __Zn.Env.TYPE_UPLOAD.CARD !== $listType
        ? "primary" : "default";
    btnList.onClick = Op.rxVList(reference);

    const btnClean = {}
    // v4
    btnClean.icon = __Zn.v4Icon("delete");
    // btnClean.type = "danger";
    btnClean.danger = true;
    btnClean.type = "primary";
    btnClean.onClick = Op.rxVClean(reference);
    btnClean.disabled = 0 === fileList.length;

    return (
        <Row className={"op-bar"}>
            <Col>
                <Button.Group className={"action"}>
                    <Tooltip title={info['vSave']}>
                        <Button {...btnClean}/>
                    </Tooltip>
                    <Tooltip title={info['vCard']}>
                        <Button {...btnCard}/>
                    </Tooltip>
                    <Tooltip title={info['vList']}>
                        <Button {...btnList}/>
                    </Tooltip>
                </Button.Group>
                <Tag color={"magenta"}>
                    {info['iTip']}
                </Tag>
            </Col>
        </Row>
    )
}
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    renderBar,
    renderFile
}