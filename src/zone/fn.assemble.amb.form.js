import __Of from './o.silver.bullet.of';

const ambFormSubmit = (reference) => {
    const {$submitting} = reference.props;
    if (undefined === $submitting) {
        return __Of.of(reference).submitting().future();
    } else {
        return __Of.of(reference)._.submitting();
    }
}

const ambFormEnd = (reference) => {
    const {$submitting} = reference.props;
    if (undefined === $submitting) {
        return __Of.of(reference).submitted().future();
    } else {
        return __Of.of(reference)._.submitted();
    }
}

export default {
    ambFormSubmit,
    ambFormEnd,
}