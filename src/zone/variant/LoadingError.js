import React from 'react';
import {Result} from 'antd';

export default (props = {}) => {
    return (
        <Result status={"error"}>
            <div className={"ux_error"}>
                {props.$message}
            </div>
        </Result>
    )
}