import func_arrange_value_complex from './fn.arrange.value.complex';
import func_arrange_value_stdn from './fn.arrange.value.stdn';
import func_arrange_element_tree from './fn.arrange.element.tree';

import func_assemble_amb_form from './fn.assemble.amb.form';
import func_assemble_amb_polysemy from './fn.assemble.amb.polysemy';
import func_assemble_element_calculate from './fn.assemble.element.calculate';
import func_assemble_element_change from './fn.assemble.element.change';
import func_assemble_element_qr from './fn.assemble.element.qr';

import func_assort_value_typed from './fn.assort.value.typed';
import func_assort_value_datetime from './fn.assort.value.datetime';
import func_assort_value_expr from './fn.assort.value.expr';

import func_atomic_async from './fn.atomic.async';
import func_atomic_foundation from './fn.atomic.foundation';

import func_under_is_business from './fn.under.is.business';
import func_under_is_complex from './fn.under.is.complex';
import func_under_is_decision from './fn.under.is.decision';
import func_under_is_rule from './fn.under.is.rule';

import func_under_it_complex from './fn.under.it.complex';
import func_under_it_spread from './fn.under.it.spread';

import func_under_kv_future from './fn.under.kv.future';
import func_under_to_business from './fn.under.to.business';
import func_under_to_expr from './fn.under.to.expr';
import func_under_to_typed from './fn.under.to.typed';

import func_under_way_io from './fn.under.way.io';

import func_debug_fx_error from './fn.debug.fx.error';
import func_debug_dg_develop from './fn.debug.dg.develop';
import func_debug_dg_logging from './fn.debug.dg.logging';
import func_debug_dgl_complex from './fn.debug.dgl.complex';

import func_web_compile_store from './fn.web.compile.store';
import func_web_connect_event from './fn.web.connect.event';
import func_web_from_react from './fn.web.from.react';
import func_web_on_react from './fn.web.on.react';

import func_unity_format from './fn.unity.format';
import func_unity_random from './fn.unity.random';
import func_unity_sorter from './fn.unity.sorter';
import func_unity_sorter_fn from './fn.unity.sorter.fn';
import func_unity_encrypt from './fn.unity.encrypt';
import func_unity_math from './fn.unity.math';

import o_bullet_fn from './o.silver.bullet.fn';
import o_bullet_of from './o.silver.bullet.of';

import value_env from './v.environment';

import value_modello_type from "./v.modello.type";
import value_modello_op from './v.modello.op';

import value_feature_key from './v.feature.key';
import value_feature_http from './v.feature.http';
import value_feature_symbol from './v.feature.symbol';
import value_feature_develop from './v.feature.develop';

import value_web_layout from './v.web.layout';
import value_web_theme from './v.web.theme';

import func_ant4_v4_icon from './fn.antd4.v4.icon';

import o_v4_component from './variant-v4-hook';

export default {
    /*
        valueAppend,
        valueValid,
        valueCopy,
        valueOk,
        valueFind,
        valueLink,
        valueMap,
     */
    ...func_arrange_value_complex,
    /*
        valueT,
        valueSTDN,
     */
    ...func_arrange_value_stdn,
    /*
        elementBranch,
        elementParent,
        elementChildren,
        elementChildTree,
     */
    ...func_arrange_element_tree,
    /*
        ambArray,
        ambKv,
        ambValue,
        ambObject,
        ambFind,
        ambEvent,
        ambAnnex,
     */
    ...func_assemble_amb_form,
    ...func_assemble_amb_polysemy,
    /*
        elementJoin,
        elementGrid,
        elementConcat,
        elementFlip,
        elementFlat,
        elementWrap,
     */
    ...func_assemble_element_calculate,
    /*
        elementSave,
        elementOrder,
        elementUp,
        elementDown,
     */
    ...func_assemble_element_change,
    ...func_assemble_element_qr,
    /*
        elementFind,
        elementUnique,
        elementGroup,
        elementGet,
        elementMap,
        elementIndex,
        elementFirst,
        elementVertical,

        elementInAll
        elementInAny
     */
    ...func_assort_value_typed,
    /*
        valueDatetime,
        valueJDatetime,
        valueDuration,
        valueStartTime,
        valueEndTime,
        valueNow,
     */
    ...func_assort_value_datetime,
    ...func_assort_value_expr,

    ...func_atomic_async,
    ...func_atomic_foundation,

    ...func_under_is_business,
    ...func_under_is_decision,
    ...func_under_is_complex,
    ...func_under_is_rule,

    ...func_under_it_complex,
    ...func_under_it_spread,

    ...func_under_kv_future,
    ...func_under_to_business,
    ...func_under_to_expr,
    ...func_under_to_typed,
    ...func_under_way_io,

    ...func_ant4_v4_icon,

    ...func_debug_fx_error,
    ...func_debug_dg_develop,
    ...func_debug_dg_logging,
    ...func_debug_dgl_complex,

    // React / Html
    ...func_web_compile_store,
    ...func_web_connect_event,
    ...func_web_from_react,
    ...func_web_on_react,

    // ---- Unity
    ...func_unity_format,
    ...func_unity_encrypt,
    ...func_unity_random,
    ...func_unity_sorter,
    ...func_unity_sorter_fn,
    ...func_unity_math,

    // High Order Function
    ...o_bullet_fn,
    ...o_bullet_of,

    ...o_v4_component,
    Env: {
        ...value_env,

        ...value_modello_type,
        ...value_modello_op,

        ...value_feature_key,
        ...value_feature_http,
        ...value_feature_symbol,
        ...value_feature_develop,

        ...value_web_layout,
        ...value_web_theme,
    }
}