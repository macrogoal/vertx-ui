import func_boundary_ui_container from './boundary.fn.ui.container';
import func_boundary_web_segment from './boundary.fn.web.segment'
import func_boundary_yl_channel_control from './boundary.fn.yl.channel.control';

import func_channel_commerce_yi_region from './channel.commerce.fn.yi.region';
import func_channel_commerce_yo_finance from './channel.commerce.fn.yo.finance';

import func_levy_out_economy from './levy.fn.out.economy';
import func_lkway_on_event from './lkway.fn.on.event';

import o_silver_bullet_designer from './o.silver.bullet.designer';
import o_silver_bullet_dialog from './o.silver.bullet.dialog';
import o_silver_bullet_form from './o.silver.bullet.form';
import o_silver_bullet_init from './o.silver.bullet.init';

import config_variant_op from './variant-op';
import config_variant_input from './variant-input';

import func_rbac_acl_action from './rbac.fn.acl.action';
import func_rbac_admit_region from './rbac.fn.acl.admit.region';
import func_rbac_child_execute from './rbac.fn.acl.child.execute';

import func_rbac_auth_remote_future from './rbac.fn.auth.remote.future';
// Areo
import areo_zero_index from './aero.zero.index';

export default {

    ...func_boundary_ui_container,
    ...func_boundary_web_segment,
    ...func_boundary_yl_channel_control,

    ...func_channel_commerce_yi_region,
    ...func_channel_commerce_yo_finance,

    ...func_levy_out_economy,
    ...func_lkway_on_event,

    // High Order
    ...o_silver_bullet_designer,
    ...o_silver_bullet_dialog,
    ...o_silver_bullet_form,
    ...o_silver_bullet_init,

    // Configuration for Variant
    ...config_variant_op,
    ...config_variant_input,

    // Secure
    ...func_rbac_acl_action,
    ...func_rbac_admit_region,
    ...func_rbac_child_execute,

    // RBAC
    ...func_rbac_auth_remote_future,

    // 引擎核心
    ...areo_zero_index,
}