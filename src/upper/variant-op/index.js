import $opLogin from './event.__.@fn.op.login';
import $opLogout from './event.__.@fn.op.logout';

export default {
    // .Op variable
    Op: {
        $opLogin,
        $opLogout,
    }
}