import _login_index from './login/index/UI';
import _main_index from './main/index/UI';
import _extension from '../extension/components';
import _development from '../extension/cerebration';

export default {
	_login_index,
	_main_index,
	..._extension,
	..._development,
}
