import React from "react";
import {createRoot} from "react-dom/client";
import {Provider} from "react-redux";
import {App, ConfigProvider} from 'antd';
import {BrowserRouter} from "react-router-dom";

import routes from "./environment/routes";
import storeGlobal from "./environment/store";

// antd组件 4.0 全套修订
// // Must import .less instead of .css because the theme should be modified here
// The old code is:   import 'antd/dist/antd.min.css';
// The old code problem is the theme could not be calculated by the system
// import 'antd/dist/antd.less';
import './style/index.scss';
import Sk from 'skin';
/**
 * @typedef JObject 从Json文件中读取的JsonObject
 * @typedef EmptyActionCreator redux-act 的返回值
 * @typedef ReactComponent React组件引用，通常是 reference
 * @typedef WebColor 值为 #XXXXXX 的Web颜色值（字符串格式）
 * @typedef Ux 全局工具类，核心调用入口
 */
/**
 * @overview
 *
 * # Zero UI框架文档
 *
 * > 标准`node`环境可运行在` > 8.x `中，目前系统环境运行在`16.14.2`，由于`17.x+`中有很多问题，所以标准环境中不推荐升级到 `17.x+`。
 *
 * 从2018年框架诞生开始，整个框架经历了很多改动，整个框架分为以下几部分：
 *
 * ## 0. 框架结构
 *
 * ### 0.1. 核心库清单
 *
 * |库名称|分类|说明|
 * |---|---|---|
 * |ux|函数类|框架核心功能库，全称为`Universe X`，封装了所有第三方库，以函数前缀进行区分。|
 * |ex|函数类|扩展核心功能库，全称为`Extension X`，封装了带有业务的底层库。|
 * |web|组件类|标准组件类型（不带任意业务逻辑，纯技术类组件），使用简化方式导入：`import { xxx } from 'web'。|
 * |ei|组件类|扩展组件类型（带一定的业务逻辑，非纯技术类组件），带有一定主观性，大部分组件和Zero Extension直接对接。|
 * |oi|组件类|配置组件库，全称为`Origin Intelligence`，用于特殊的后端配置专用加载库，这种模式的组件主要模式为：前端容器 + 后端组件。|
 * |ui|组件类|快速组件开发库，全称为`User Intelligence`，封装了常用的组件（经实战检验过）的快速开发类API。|
 * |app|用户类|「项目差异」用户自定义专用库，可开发根据项目不同的自定义库相关信息。|
 * |plugin|用户类|「项目差异」插件开发库，可开发需要使用的插件相关信息。|
 * |mock|用户类|「项目差异」前端模拟库，脱离后端进行开发时需使用的模拟远程专用库（和API对应）。|
 * |lang|系统类|语言专用库，用于存储不同语言级的资源文件，具体读取的资源文件根据环境变量`Z_LANGUAGE`指定。|
 * |entity|系统类|数据模型库，底层使用TypeScript编写的数据模型，牵涉Zero Ui中的核心数据模型，为底层运行提供了可靠一句。|
 * |environment|系统类|系统环境库，主要包含`zero`注解，自动路由、自动行为等核心库信息。|
 *
 * ### 0.2. 特殊说明
 *
 * * `oi / ui`二者配合可形成一智能开发平台，开发模式如：
 *      * 纯开发模式，借用`ui`可快速开发完整模块（通用的列表/表单搭配模块），低代码开发模式。
 *      * 纯配置模式，借用`oi`和后端`zero-ui`模块对接，实现零代码开发开发模式。
 *      * 如果有自定义逻辑，则可以在原始API中扩展。
 * * `mock`替代了常用的mock-server，仅提供数据结构实现数据模拟行为，模拟过程中会自动连接浏览器中的SQLLite实现纯模拟
 *      * 只有开发模式「Development」可打开模拟行为（生产模式禁用）。
 *      * 在开发模式中可针对您编写的服务端接口实现单接口模拟和远程接口混搭模拟的方式，单独调试方便。
 * * `app / plugin`：自定义库开发，自由发挥，通常是同一个项目中需要重用的信息放到`app`中，而插件则是纯配置模式无法处理时启用。
 *
 * ### 0.3. 文档站点
 *
 * * 主文档：
 *      * 本地地址：<http://localhost:1017>
 *      * 外网地址：<http://www.vertxui.cn/document/doc-web/index.html>
 * * 扩展文档：
 *      * 本地地址：<http://localhost:1231>
 *      * 外网地址：<http://www.vertxui.cn/document/doc-web-extension/index.html>
 *
 * **您可点击左边的完整菜单直接导航到外网地址中，本地地址则不可如此用法。**
 *
 * ### 0.4. 编程使用
 *
 * ```js
 *
 * // Zero UI 工具包使用方式
 * import Ux from 'ux';
 *
 * // Zero UI Component 标准组件使用，如：GraphicEditor
 * import {GraphicEditor} from 'web';
 *
 * ```
 *
 * 所有的工具都直接走`Ux.xxx`的方式执行调用，上层调用中，不用考虑类相关信息，除开类类型的特殊调用如：`Ux.Ant.xxx`以外，剩余部分都是直接调用，开发人员则可以根据函数前缀来判断API的用法，在文档中也会针对不同的API执行函数说明。
 *
 * ## 1. 文档配置环境
 *
 * ```shell
 * # 新版使用 yarn 替代了 npm，所以安装可使用新方式
 * yarn add docdash --dev
 * # 旧：npm install docdash
 *
 * # 如果缺乏 jsdoc，则直接运行
 * yarn add jsdoc --dev
 * # 旧：npm install jsdoc
 * ```
 *
 * 由于项目中本身不存在上述的 dependency 的第三方依赖包，您可以直接安装，也可以加上`-g`参数安装在全局环境中，安装完成后，直接执行根目录下的 `run-doc.sh` 脚本生成相关文档。
 *
 * ## 2. 文档服务器
 *
 * 如果您要使用文档服务器，先在机器上安装`live-server`（建议使用全局模式）：
 *
 * ```shell
 * yarn global add live-server
 * # 旧：npm install live-server -g
 * ```
 *
 * ## 3. 运行服务器
 *
 * 安装完成后，直接执行根目录下的 `/run-doc.sh` 脚本则可执行文档服务器。
 *
 * |命令执行|参数名|含义|
 * |---:|:---|:---|
 * |`./run-doc.sh doc`|doc|使用文档API生成文档，文档存储于document目录中，doc-web为标准文档，doc-web-extension为扩展文档。|
 * |`./run-doc.sh zero`|zero|运行标准文档，Ux。|
 * |`./run-doc.sh extension`|extension|运行扩展文档，Ex。|
 *
 * ```shell
 * // 生成 Zero UI/Extension 文档
 * ./run-doc.sh doc
 *
 * // 运行 Zero UI 文档
 * ./run-doc.sh zero
 * ```
 *
 * ## 4. 基本规则
 *
 * ### 4.1. 基本使用
 *
 * 1. Zero Ui模块中的函数全部使用 `Ux.xxx` 的方式调用。
 * 2. Zero Ui Extension模块中的函数全部使用 `Ex.xxx` 的方式调用。
 *
 * ### 4.2. 关于标记
 *
 * 如果有特殊函数，整个文档会使用加粗的方式标记该函数，主要标记如下：
 *
 * * 「标准」JavaScript标准封装函数（脱离Zero Ui可使用）。
 * * 「引擎」Zero Ui中的核心层函数，提高开发效率专用。
 * * 「类」Zero Ui中定义的核心类函数。
 * * 「私有」内部函数，不对外提供API，不可使用`Ux.xxx`方式调用。
 *
 * ### 4.3. 基本标记规则
 *
 * * 方法定义之前有 `(inner)` 修饰标记，这种类型的方法都可以直接通过 `Ux.xxx` 的方式调用。
 * * 方法定义中的参数 `(arguments)` 为可变长度参数，引用`arguments`默认JS参数处理，且定义方式使用`ES5`中的`function(){}`方式。
 * * 如果方法返回值中返回`{Promise.<T>}` 则表示该方法支持**同步**和**异步**两种，且返回值为Promise，如果是异步函数则会带上`async`标记。
 * * 方法定义之前有 `(static)` 修饰标记，则属于类中静态方法，可直接调用。
 *
 * ### 4.4. 章节说明
 *
 * 整个代码说明区域主要包含几个章节
 *
 * * 标准函数：在整个 JavaScript 的运行时可用。
 * * 引擎函数：位于`engine`专用库中的函数入口，特定引擎函数，带有引擎逻辑，注`unity`中还包含引擎函数。
 *
 * 所有代码中都存在的部分
 *
 * * 参数函数：在调用的API中有特殊的`Function`类型的函数参数说明。
 * * 技巧：演示新旧函数调用的核心技巧，带上【巧】备注。
 *
 * ## 5. 总结
 *
 * 如果您有什么疑问，请联系：[silentbalanceyh@126.com](mailto:silentbalanceyh@126.com)，整体框架相关链接参考左侧菜单。
 * @author 戒子猪
 */
const container = document.getElementById("root");
const root = createRoot(container);
root.render(
    <ConfigProvider {...Sk.skinDefaultProvider()}>
        <App>
            <Provider store={storeGlobal}>
                <BrowserRouter store={storeGlobal}>
                    {routes}
                </BrowserRouter>
            </Provider>
        </App>
    </ConfigProvider>
)
