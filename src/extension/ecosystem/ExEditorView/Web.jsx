import {Checkbox, Col, Popconfirm, Radio, Row} from "antd";
import React from "react";
import Ux from 'ux';
import Op from './Op';
import {Link} from "react-router-dom";

const renderItem = (reference) => (item) => {
    const {$myDefault = {}} = reference.props;
    let title = item.title;
    if (!title && $myDefault.name === item.name) {
        title = $myDefault.title;
    }
    const {
        $combine = {}, $selectedKeys = [],
        $selectedKey
    } = reference.state;
    const {toolbar = {}} = $combine;
    const itemAttr = {};
    if (Op.isEdit(reference)) {
        itemAttr.onDoubleClick = Op.rxOpen(reference, item);
    } else {
        itemAttr.onClick = Op.rxRadio(reference, item);
    }
    return (
        <Row className={"v-list-item"} {...itemAttr}>
            <Col span={2}>
                {Op.isEdit(reference) ? (
                    <Checkbox onChange={Op.rxChecked(reference, item)}
                              checked={$selectedKeys.includes(item.key)}/>
                ) : false}
                {Op.isView(reference) ? (
                    <Radio name={"rdoView"} checked={$selectedKey === item.name}
                           onChange={Op.rxRadio(reference, item)}/>
                ) : false}
            </Col>
            <Col span={10}>
                {title}（{item.name}）
            </Col>
            <Col span={12} className={"v-list-right"}>
                {(() => {
                    const {edit = {}} = toolbar;
                    return Op.isEdit(reference) ? (
                        <Link onClick={Op.rxOpen(reference, item)} to={""}>
                            {Ux.v4Icon(edit.icon)}
                            &nbsp;{edit.text}
                        </Link>
                    ) : false;
                })()}
                &nbsp;&nbsp;&nbsp;&nbsp;
                {(() => {
                    const {remove = {}} = toolbar;
                    return Op.isEdit(reference) && "DEFAULT" !== item.name ? (
                        <Popconfirm title={remove.confirm}
                                    onConfirm={Op.rxDelete(reference, item, remove)}>
                            <Link to={""} onClick={event => Ux.prevent(event)}>
                                {Ux.v4Icon(remove.icon)}
                                &nbsp;{remove.text}
                            </Link>
                        </Popconfirm>
                    ) : false;
                })()}
            </Col>
        </Row>
    );
}
const renderHeader = (reference, header) => {
    return (
        <div className={"checked-all"}>
            {header}
        </div>
    )
}
export default {
    // List中的Item渲染
    renderItem,
    renderHeader,
}