import __Zu from 'zet';
import __Zp from 'zep';

export default {
    // 结算单初始化
    inSettlement: (data = {}) =>
        __Zu.inSettlement(data),
    // 退款 / 应收初始化
    inDebt: (reference, $inited = {}, debt = true) =>
        __Zp.yoDebt(reference, $inited, debt),
    yoDebt: (reference, $inited = {}, debt = true) =>
        __Zp.yoDebt(reference, $inited, debt),
}