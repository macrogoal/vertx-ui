import __Zn from './zero.module.dependency';
import __Tt from './tree.fn.to.configuration';
import __Vt from './vow.__.fn.u.zone';

const _matrix = (array = [], object = {}, fnExecute, fnPredicate) => {
    if (!__Zn.isEmpty(object)) {
        // 是否检查
        const predicate = __Zn.isFunction(fnPredicate) ? fnPredicate : () => true;
        __Zn.itFull(array, object, (item = {}, key, value) => {
            if (predicate(value)) {
                fnExecute(item, key, value);
            }
        });
    }
};

class Uarr {
    constructor(data = []) {
        /*
         * 拷贝节点，原始节点不改变，
         * Stream操作无副作用
         */
        this.data = __Zn.clone(data);
    }

    static create(data = []) {
        __Zn.fxTerminal(!__Zn.isArray(data), 10057, data);
        if (__Zn.isArray(data)) {
            return new Uarr(data);
        }
    }

    matrix(items = {}) {
        _matrix(this.data, items, (item = {}, key, value) => {
            const id = item[key];
            const found = __Zn.elementUnique(value, "key", id);
            if (found) {
                item[key] = found;
            }
        }, __Zn.isArray);
        return this;
    }

    mount(reference) {
        this.reference = reference;
        return this;
    }

    each(applyFun) {
        __Vt.uEach(this.data, applyFun);
        return this;
    }

    remove(...attr) {
        __Vt.uCut.apply(this, [this.data].concat(attr));
        return this;
    }

    slice(...keys) {
        const reference = this.data;
        this.data = __Zn.slice.apply(this, [reference].concat(keys));
        return this;
    }

    debug() {
        __Zn.dgDebug(this.data, "Uarr 调试");
        return this;
    }

    mapping(mapping = {}, override = false) {
        const result = [];
        this.data.forEach(item => result.push(__Vt.uExpand(item, mapping, override)));
        this.data = result;
        return this;
    }

    filter(func) {
        if (__Zn.isFunction(func)) this.data = this.data.filter(func);
        return this;
    }

    convert(field, func) {
        if (field && __Zn.isFunction(func)) __Zn.itElement(this.data, field, func);
        return this;
    }

    add(field, any) {
        if (field) this.data.forEach(item => item[field] = __Zn.isFunction(any) ? any(item) : any);
        return this;
    }

    sort(func) {
        if (__Zn.isFunction(func)) this.data = this.data.sort(func);
        return this;
    }

    map(func) {
        if (__Zn.isFunction(func)) this.data = this.data.map(func);
        return this;
    }

    to() {
        return this.data;
    }

    tree(config = {}) {
        this.data = __Tt.toTree(this.data, config);
        return this;
    }
}

export default Uarr;