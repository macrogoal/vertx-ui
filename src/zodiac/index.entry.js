import func_antd4_html_effective from './antd4.fn.html.effective';
import func_antd4_message_reply from './antd4.fn.message.reply';
import func_antd4_v4_patch from './antd4.fn.v4.patch';
import func_antd4_v4_patch_form from './antd4.fn.v4.patch.form';

import func_form_form_action from './form.fn.form.action';
import func_form_to_setting from './form.fn.to.setting';
import func_form_write_dependency from './form.fn.write.dependency';

import func_lighting_ajax_callback from './lighting.fn.ajax.callbak';
import func_lighting_ajax_standard from './lighting.fn.ajax.standard';
import func_lighting_ajax_stream from './lighting.fn.ajax.stream';
import func_lighting_async_callback from './lighting.fn.async.callback';
import func_lighting_micro_legacy from './lighting.fn.micro.legacy';
import func_lighting_sock_stomp from './lighting.fn.sock.stomp';

import func_lkway_amb_polysemy from './lkway.fn.amb.polysemy';
import func_lkway_on_under_prop from './lkway.fn.on.under.prop';
import func_lkway_yo_under_prop from './lkway.fn.yo.under.prop';
import func_lkway_rx_event from './lkway.fn.rx.event';

import func_secure_acl_authorization from './secure.fn.acl.authorization';
import func_secure_digit_signature from './secure.fn.digit.signature';

import func_source_datum_assist_io from './source.datum.fn.assist.io';
import func_source_datum_element_qr from './source.datum.fn.element.qr';
import func_source_datum_on_consumer from './source.datum.fn.on.consumer';
import func_source_datum_parse_data from './source.datum.fn.parse.data';
import func_source_datum_value_fabric from './source.datum.fn.value.fabric';
import func_source_parse_expression from './source.fn.parse.expression';
import func_source_parse_extension from './source.fn.parse.extension';
import func_source_parse_transformer from './source.fn.parse.transformer';
import func_source_write_redux from "./source.fn.write.redux";

import func_store_is_configuration from './store.fn.is.configuration';
import func_store_store_application from './store.fn.store.application';

import func_syntax_expr_action from './syntax.fn.ai.expr.action';
import func_syntax_expr_container from './syntax.fn.ai.expr.container';
import func_syntax_expr_content from './syntax.fn.ai.expr.content';
import func_syntax_expr_control from './syntax.fn.ai.expr.control';
import func_syntax_apply_attribute from './syntax.fn.apply.attribute';
import func_syntax_apply_component from './syntax.fn.apply.component';
import func_syntax_apply_rule from './syntax.fn.apply.rule';
import func_syntax_parse_component from './syntax.fn.parse.component';

import func_tree_forest_build from './tree.fn.forest.build';
import func_tree_to_configuration from './tree.fn.to.configuration';
import func_tree_tree_selection from './tree.fn.tree.selection';

import func_vow_dsl_definition from './vow.fn.dsl.definition';

export default {
    // AntV4属性兼容器
    ...func_antd4_html_effective,       // 效果
    ...func_antd4_message_reply,        // 提示消息
    ...func_antd4_v4_patch,             // V4 升级补丁
    ...func_antd4_v4_patch_form,        // V4 表单升级补丁

    ...func_form_form_action,           // 表单行为
    ...func_form_to_setting,            // 表单设置
    ...func_form_write_dependency,      // 表单依赖
    // 远程通信
    ...func_lighting_ajax_callback,     // 回调
    ...func_lighting_ajax_standard,     // 标准
    ...func_lighting_ajax_stream,       // 二进制
    ...func_lighting_async_callback,    // 回调功能
    ...func_lighting_micro_legacy,      // 微服务
    ...func_lighting_sock_stomp,        // 主动
    // 通道层
    ...func_lkway_amb_polysemy,
    ...func_lkway_on_under_prop,
    ...func_lkway_yo_under_prop,
    ...func_lkway_rx_event,

    // 数字签名模块
    ...func_secure_acl_authorization,
    ...func_secure_digit_signature,
    // 数据字典计算器
    ...func_source_datum_assist_io,
    ...func_source_datum_element_qr,
    ...func_source_datum_on_consumer,
    ...func_source_datum_parse_data,
    ...func_source_datum_value_fabric,
    ...func_source_parse_expression,
    ...func_source_parse_extension,
    ...func_source_parse_transformer,
    ...func_source_write_redux,
    // 浏览器存储
    ...func_store_is_configuration,
    ...func_store_store_application,
    // 属性解析（语法解析）
    ...func_syntax_expr_action,
    ...func_syntax_expr_container,
    ...func_syntax_expr_content,
    ...func_syntax_expr_control,
    ...func_syntax_apply_attribute,
    ...func_syntax_apply_component,
    ...func_syntax_apply_rule,
    ...func_syntax_parse_component,
    // 树型计算
    ...func_tree_forest_build,
    ...func_tree_to_configuration,
    ...func_tree_tree_selection,
    // 未来放弃
    ...func_vow_dsl_definition
}