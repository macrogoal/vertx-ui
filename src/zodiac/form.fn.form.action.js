import __Zn from './zero.module.dependency';
import __Fr from './antd4.fn.v4.patch';

const formGet = (reference, key = undefined, warningOn = true) => {
    if (reference) {
        // E.fxTerminal(!form, 10020, form);
        // 旧代码：const {form} = reference.props;
        const form = __Fr.v4FormRef(reference);
        if (form) {
            let data = form.getFieldsValue();
            data = __Zn.clone(data);
            if (__Zn.isArray(key)) {
                const result = {};
                key.forEach(each => result[each] = data[each]);
                return __Zn.valueValid(result);
            } else {
                return key ? data[key] : data;
            }
        } else {
            if (warningOn) {
                console.warn("[V4] Form 引用读取失败！！", form, key);
            }
        }
    }
};

const formReset = (reference, keys = [], response = {}) => {
    // 旧代码：const {form} = reference.props;
    const form = __Fr.v4FormRef(reference);
    if (form) {
        if (0 < keys.length) {
            form.resetFields(keys);
        } else {
            form.resetFields();
        }
        __Zn.fn(reference).rxReset(reference, keys, response);
    } else {
        const ref = __Zn.onReference(reference, 1);
        const refForm = __Fr.v4FormRef(ref);
        if (refForm) {
            formReset(reference, keys, response);
        }
    }
};
const formHit = (reference, key, value = undefined) => {
    // 旧代码：const {form} = reference.props;
    const form = __Fr.v4FormRef(reference);
    __Zn.fxTerminal(!form, 10020, form);
    if (form) {
        if (undefined !== value) {
            const values = {};
            values[key] = value;
            form.setFieldsValue(values);
            //__Zn.of(reference).up().done();
        } else {
            return form.getFieldValue(key);
        }
    }
};
const formHits = (reference, values = {}) => {
    // 旧代码：const {form} = reference.props;
    const form = __Fr.v4FormRef(reference);
    __Zn.fxTerminal(!form, 10020, form);
    // 无form引用
    if (!form) {
        return;
    }
    // 无值处理
    if (__Zn.isEmpty(values)) {
        return;
    }
    // 判断是否发生改变
    const fields = Object.keys(values);
    const valueOld = formGet(reference, fields);
    // 防止死循环，执行深度判断设值（原始值和现有值执行比对）
    if (__Zn.isDiff(valueOld, values)) {
        const fields = Object.keys(form.getFieldsValue());
        // Fix: You cannot set a form field before rendering a field associated with the value.
        const formValues = __Zn.clone(values);
        Object.keys(values).forEach(field => {
            if (!fields.includes(field)) {
                delete formValues[field];
            }
        });
        form.setFieldsValue(formValues);
        //__Zn.of(reference).up().done();
    } else {
        __Zn.dgDebug({form}, "新旧值未发生该变，不触发！！")
    }
};
const formClear = (reference, data) => {
    const {$clear} = reference.props;
    // 旧代码：const {form} = reference.props;
    const form = __Fr.v4FormRef(reference);
    // 记录切换：从更新表单 -> 添加表单切换
    if ($clear && $clear.is()) {
        // 记录切换：从更新某条记录 -> 更新另外一条记录
        const keys = $clear.to();
        keys.forEach(key => __Zn.valueAppend(data, key, undefined));
    }
    const fields = Object.keys(form.getFieldsValue());
    fields.forEach(key => __Zn.valueAppend(data, key, undefined));
    form.setFieldsValue(data);
    //__Zn.of(reference).up().done();
};
export default {
    // 读取：data[key] | data
    formGet,
    // 充值
    formReset,
    // 「二义性」读 / 写
    formHit,
    // 「批量」写
    formHits,
    // 清空
    formClear,
}
