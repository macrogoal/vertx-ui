import func_annotation_anno_util from './annotation.fn.anno.util';
import func_annotation_anno_error from './annotation.fn.anno.error';

import func_debugger_dev_switcher from './debugger.fn.dev.switcher';

import func_lkway_on_web_prop from './lkway.fn.on.web.prop';
import func_lighting_ajax_uca from './lighting.fn.ajax.uca';
import func_lighting_async_commerce from './lighting.fn.async.commerce';

import func_page_ai_layout_norm from './page.fn.ai.layout.norm';
import func_page_to_rect from './page.fn.to.rect';
import func_page_to_theme from './page.fn.to.theme';

import func_query_qr_interact_form from './query.fn.qr.interact.form';
import func_query_qr_interact_element from './query.fn.qr.interact.element';
import func_query_qr_operation_unit from './query.fn.qr.operation.unit';
import func_query_qr_syntax_calculate from './query.fn.ir.syntax.calculate';
import func_query_qr_syntax_parser from './query.fn.qr.syntax.parser';

import func_redux_$_epic_observable from './redux.fn._.epic.observable';
import func_redux_data_reducer from './redux.fn.data.reducer';
import func_redux_fixbug from './redux.fn.is.fixbug';
import func_redux_rx_observable from './redux.fn.rx.observable';

import func_router_is_locate from './router.fn.is.locate';
import func_router_to_location from './router.fn.to.location';
import func_router_to_parameter from './router.fn.to.parameter';

import func_uca_xt_control_form from './uca.fn.xt.control.form';
import func_uca_xt_control_row from './uca.fn.xt.control.row';
import func_uca_xt_control_upload from './uca.fn.xt.control.upload';

import func_uca_xt_economy_dialog from './uca.fn.xt.economy.dialog';
import func_uca_xt_economy_search from './uca.fn.xt.economy.search';
import func_uca_xt_economy_table from './uca.fn.xt.economy.table';

import func_uca_xt_expr_parser from './uca.fn.xt.expr.parser';
import func_uca_xt_init_typed from './uca.fn.xt.init.typed';
import func_uca_xt_inited_values from './uca.fn.xt.inited.values';
import func_uca_xt_lazy_loading from './uca.fn.xt.lazy.loading';
import func_uca_xt_lazy_ready from './uca.fn.xt.lazy.ready';
import func_uca_xt_op_event from './uca.fn.xt.op.event';
import func_uca_xt_op_primary from './uca.fn.xt.op.primary';

import func_verify_valve_validator from './verify.fn.valve.validator';

import func_web_pin_active_in from './web-pin.fn.active.in';
import func_web_pin_anchor_out from './web-pin.fn.anchor.out';

import func_window_rx_event from './window.fn.rx.event';
import o_window_bullet from './window.o.silver.bullet.wl';

import v_web_icon_set from './v.web.icon.set';
import v_verify_constant from './v.verify.constant';
import v_limitation from './v.uca.limitation';
import v_economy_business from './v.economy.business';

import future from './web-loader.@fn.future';

export default {
    ...func_annotation_anno_util,
    ...func_annotation_anno_error,

    ...func_debugger_dev_switcher,
    ...func_lkway_on_web_prop,

    ...func_lighting_ajax_uca,
    ...func_lighting_async_commerce,

    ...func_query_qr_interact_form,
    ...func_query_qr_interact_element,
    ...func_query_qr_operation_unit,
    ...func_query_qr_syntax_calculate,
    ...func_query_qr_syntax_parser,

    ...func_redux_$_epic_observable,
    ...func_redux_data_reducer,
    ...func_redux_fixbug,
    ...func_redux_rx_observable,

    ...func_router_is_locate,
    ...func_router_to_location,
    ...func_router_to_parameter,

    ...func_uca_xt_control_form,
    ...func_uca_xt_control_row,
    ...func_uca_xt_control_upload,

    ...func_uca_xt_economy_dialog,
    ...func_uca_xt_economy_search,
    ...func_uca_xt_economy_table,

    ...func_uca_xt_expr_parser,
    ...func_uca_xt_init_typed,
    ...func_uca_xt_inited_values,
    ...func_uca_xt_lazy_loading,
    ...func_uca_xt_lazy_ready,
    ...func_uca_xt_op_event,
    ...func_uca_xt_op_primary,

    ...func_verify_valve_validator,

    ...func_page_ai_layout_norm,
    ...func_page_to_rect,
    ...func_page_to_theme,

    ...func_web_pin_active_in,
    ...func_web_pin_anchor_out,

    ...func_window_rx_event,
    ...o_window_bullet,

    future,

    Env: {
        ...v_verify_constant,
        ...v_web_icon_set,
        ...v_limitation,
        ...v_economy_business,
    }
}