import React from 'react';
import {Drawer} from 'antd';
import Ux from "ux";
import {LoadingAlert} from "web";

import "./Cab.norm.scss";
import __Zn from '../zero.aero.dependency';

const _renderNotice = (reference) => {
    const {$notice} = reference.state;
    return $notice ? (
        <LoadingAlert $alert={$notice} $type={"warning"}/>
    ) : false;
};
export default (reference) => {
    const {
        $advanced,
        $visible = false
    } = reference.state;
    // v4
    $advanced.open = $visible;
    const {$form = {}} = reference.props;
    const {FormFilter} = $form;
    if (FormFilter) {
        const filterAttrs = __Zn.yoFilter(reference);
        // console.log(filterAttrs);
        filterAttrs.rxClose = (response = {}, addOn = {}) => {
            // 高级搜索命中搜索框部分执行相关操作
            let searchText = "";
            const {$fields = []} = reference.state;
            const {$condition = {}} = addOn;
            $fields.forEach(cond => {
                if (!searchText && $condition.hasOwnProperty(cond)) {
                    searchText = $condition[cond][0];
                }
            })
            const state = {searchText};
            Ux.of(reference).in(state).hide().handle(() => {
                Ux.of(reference)._.submitted({
                    $loading: true,
                }).then(() => {
                    const ref = Ux.onReference(reference, 1);
                    Ux.dglQrAdvance(ref);
                });
            })
        }
        return (
            <Drawer {...$advanced} rootClassName={"uex_ExSearch_Drawer"}>
                {/* Drawer issue: https://github.com/ant-design/ant-design/issues/20175 */}
                {_renderNotice(reference)}
                <FormFilter {...filterAttrs}/>
            </Drawer>
        );
    } else {
        console.warn("FormFilter未配置，请检查");
        return false;
    }
}