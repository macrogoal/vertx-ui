import Op from './UI.Op';
import React from 'react';
import {Button, Input, Tooltip} from 'antd';
import Ux from "ux";
import "./Cab.norm.scss";
import __Zn from '../zero.aero.dependency';

import renderDrawer from './Web.Drawer';
import renderCriteria from './QView.Web.jsx';

const _renderInput = (reference) => {
    const {$search, searchText} = reference.state;
    return (
        <Input.Search {...$search} value={searchText}/>
    )
};

const _renderTooltip = (title, fnRender) => {
    if (title) {
        return (
            <Tooltip title={title} placement={"top"}
                     destroyTooltipOnHide>
                {fnRender()}
            </Tooltip>
        )
    } else {
        return fnRender();
    }
};

const _renderRedo = (reference) => {
    const {config = {}, $condition = {}} = reference.props;
    const title = config[__Zn.Opt.SEARCH_OP_REDO];
    const attrs = {};
    attrs.icon = Ux.v4Icon("delete");
    if (0 === Object.keys($condition).length) {
        attrs.disabled = true;
    } else {
        const counter = Object.keys($condition).map(key => $condition[key])
            .filter(value => 0 < value.length);
        attrs.disabled = 0 === counter.length;
    }
    return _renderTooltip(title, () => (
        <Button onClick={Op.onClear(reference)} {...attrs}/>
    ));
};
const _renderView = (reference) => {
    const {config = {}} = reference.props;
    const view = config[__Zn.Opt.SEARCH_OP_VIEW];
    if (view) {
        const attrs = {}
        attrs.className = "uc_pink";
        return _renderTooltip(view, () => (
            <Button icon={Ux.v4Icon("fullscreen")} {...attrs}
                    onClick={Op.onShow(reference)}/>
        ));
    } else return false;
}
const _renderAdvanced = (reference) => {
    const {config = {}} = reference.props;
    const title = config[__Zn.Opt.SEARCH_OP_ADVANCED];
    const advanced = Op.isAdvanced(reference);
    return _renderTooltip(title, () => (
        <Button icon={Ux.v4Icon("filter")} htmlType={"button"}
                className={advanced ? "search-right" : "search-clean"}
                onClick={event => {
                    Ux.prevent(event);
                    Ux.of(reference).open().done();
                }}/>
    ));
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (reference) =>
    Op.isSearch(reference) ? (
        <span className={"ux_op_search"}>
            <Button id={Ux.Env.K_UI.BTN_CLEAR_SEARCH} className={"ux_hidden"}
                    onClick={event => {
                        Ux.prevent(event);
                        Ux.of(reference).in({searchText: ""}).done();
                    }}/>
            {_renderInput(reference)}
            &nbsp;&nbsp;
            <Button.Group>
                {_renderView(reference)}
                {Op.isAdvanced(reference) ? _renderRedo(reference) : false}
                {Op.isAdvanced(reference) ? _renderAdvanced(reference) : false}
                {Op.isAdvanced(reference) ? Ux.anchorSearch(reference) : false}
            </Button.Group>
            {renderCriteria(reference)}
            {Op.isAdvanced(reference) ? renderDrawer(reference) : false}
        </span>
    ) : false;