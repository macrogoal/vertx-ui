import Ux from "ux";
import __Zn from "../zero.aero.dependency";

const __qrCalc = (reference, $qrData = {}) => {
    const {$qr = {}} = reference.props;
    const updatedQr = {};
    Object.keys($qr).forEach(qrItem => {
        if ("connector" === qrItem) {
            updatedQr[""] = $qrData[""];
        } else {
            if ($qrData.hasOwnProperty(qrItem)) {
                updatedQr[qrItem] = $qrData[qrItem];
            } else {
                updatedQr[qrItem] = Ux.Env.CV_DELETE;
            }
        }
    });
    Object.keys($qrData).filter(item => !$qr.hasOwnProperty(item))
        .forEach(item => updatedQr[item] = $qrData[item]);
    return updatedQr;
}
export default {
    onViewPre: (reference) => (updated = {}) => {
        const $qrData = Ux.clone(updated);
        Ux.of(reference).in({$qrData}).done();
        // reference.?etState({$filters});
    },
    opViewSave: (reference) => (event) => {
        Ux.prevent(event);
        const {config = {}} = reference.props;
        const fnExecutor = () => {
            const {rxMyViewQ} = reference.props;
            if (!Ux.isFunction(rxMyViewQ)) {
                throw new Error("[ Ex ] 核心函数丢失：rxMyViewQ ");
            }
            let {$qrData = {}} = reference.state;
            $qrData = Ux.clone($qrData);          // 强制刷新
            if ($qrData.connector) {
                $qrData[""] = "AND" === $qrData.connector;
                delete $qrData.connector;
            }
            Ux.of(reference).submitting().handle(() => {
                rxMyViewQ($qrData).then(() => {
                    // 关闭当前窗口
                    const updatedQr = __qrCalc(reference, $qrData);
                    Ux.of(reference)._.qrSubmit(updatedQr, {
                        $spinning: true, $loading: true,
                    }).then(() => {

                        Ux.of(reference).submitted().in({$visibleQ: false}).done();
                    })
                    // 更新上层状态
                    // reference.?etState({$visibleQ: false});
                });
            })
        }
        const view = config[__Zn.Opt.SEARCH_CRITERIA_VIEW];
        if (view.confirm) {
            const md = Ux.v4Modal()
            md.confirm({
                width: 560,     // 标准文字长度
                content: view.confirm,
                onOk: fnExecutor
            })
        } else {
            fnExecutor()
        }
    },
}