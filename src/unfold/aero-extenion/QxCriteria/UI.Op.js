import Ux from "ux";

export default {
    rxDelete: (reference, item) => (event) => {
        Ux.prevent(event);
        // 成功执行
        const {value = {}} = reference.props;
        const $value = Ux.clone(value);
        if ($value.hasOwnProperty(item.fieldCond)) {
            delete $value[item.fieldCond]; //  = Ux.Env.CV_DELETE;
        }
        Ux.fn(reference).onChange($value);
    },
    rxActive: (reference) => ($activeKey) => {
        Ux.of(reference).in({$activeKey}).done()
    }
}