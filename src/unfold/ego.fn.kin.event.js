import __Zn from './zero.module.dependency';
import Ux from 'ux';
import kinTRow from './ego.__.@fn.kin.event.row';
import kinTChange from './ego.__.@fn.kin.event.change';
/*
 * 原表格内的
 * onRow
 * onChange
 */
const kinDoSearch = (reference, state = {}) => {
    const {$loading = false, $query = {}} = reference.state;
    if ($loading) {
        /*
         * 从上层抽取 rxSearch
         * 1）如果属性中已经传入了 rxSearch，则直接执行外层 rxSearch
         * 2）如果属性中没有传入 rxSearch，则构造List所需的 rxSearch
         */
        const rxSearch = __Zn.rxSearch(reference);
        return rxSearch($query).then(response => {
            /*
             * 原 ExTable 中的 onData 方法
             * 内部如果出现了 rxHoriz 方法，则针对每一行调用 rxHoriz 方法
             */
            const data = Ux.valueArray(response);
            const {rxHoriz} = reference.props;
            if (Ux.isFunction(rxHoriz)) {
                const processed = [];
                data.forEach(each => {
                    const eachRow = rxHoriz(each, reference);
                    processed.push(!!eachRow ? eachRow : each);
                });
                response.list = processed;
            } else {
                response.list = data;
            }
            state.$loading = false;     // 加载完成
            state.$spinning = false;    // 双通道处理

            if (response.__qr) {
                const $qr = Ux.clone(response.__qr);
                if ($qr.hasOwnProperty("")) {
                    $qr.connector = $qr[""] ? "AND" : "OR";
                    delete $qr[""];
                }
                Ux.dglQrV(reference, $qr, response.__qr, true);
                state.$qr = $qr;
            }
            Ux.dglApi(reference, response);
            return __Zn.yiColumn(reference, state, response);
        });
    } else {
        return Ux.promise(state);
    }
}
export default {
    kinDoSearch,
    // ExTable专用
    kinTRow,
    kinTChange
}