import F from './cell.form.__.v.configuration';

export default (reference) => ({
    __children: {
        phase: F.phase(reference),
        // 操作历史专用字段
        monitorHistory: F.monitorHistory(reference),
        // 流程图专用字段
        monitorBpmn: F.monitorBpmn(reference),
        // 关联工单
        linkageTicket: F.linkageTicket(reference),
        // 关联资产
        linkageAsset: F.linkageAsset(reference),
        // 关联员工
        linkageEmployee: F.linkageEmployee(reference),
    }
})