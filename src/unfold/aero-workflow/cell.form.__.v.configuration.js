import form_phase from './cell.form.__.@fn.phase';
import form_monitor_history from './cell.form.__.@fn.monitor.history';
import form_monitor_bpmn from './cell.form.__.@fn.monitor.bpmn';
import form_linkage_ticket from './cell.form.__.@fn.linkage.ticket';
import form_linkage_asset from './cell.form.__.@fn.linkage.asset';
import form_linkage_employee from './cell.form.__.@fn.linkage.employee';

export default {
    phase: form_phase,
    monitorHistory: form_monitor_history,
    monitorBpmn: form_monitor_bpmn,
    linkageTicket: form_linkage_ticket,
    linkageAsset: form_linkage_asset,
    linkageEmployee: form_linkage_employee,
}