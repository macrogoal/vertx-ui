import list_serial from './cell.list.__.@fn.serial';
import list_phase from './cell.list.__.@fn.phase';

export default {
    //  List
    serial: list_serial,
    phase: list_phase,
}