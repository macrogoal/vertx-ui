import L from './cell.list.__.v.configuration';

export default (reference) => ({
    // 函数模式
    serial: L.serial(reference),
    phase: L.phase(reference),
});