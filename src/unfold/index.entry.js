import func_ably_setting_yi from './ably.setting.fn.yi';
import func_ably_setting_yo from './ably.setting.fn.yo';
import func_ably_setting_yo_header from './ably.setting.fn.yo.pro.header';

import func_compile_pu_control from './compile.fn.pu.control';

import func_ego_use from './index.entry.ego';

export default {
    ...func_ably_setting_yi,
    ...func_ably_setting_yo,
    ...func_ably_setting_yo_header,
    ...func_compile_pu_control,
    ...func_ego_use,
}