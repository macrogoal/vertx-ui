import yoAmbient from './channel.@fn.yo.ambient';
import yoDynamic from './channel.@fn.yo.dynamic';
import yoRender from './channel.@fn.yo.render';

import func_channel_event_yo_action from './channel.event.fn.yo.action';

import func_channel_cm_yi_container_atom from './channel.cm.fn.yi.container.atom';
import func_channel_cm_yo_container_atom from './channel.cm.fn.yo.container.atom';
import func_channel_cm_yo_container_norm from "./channel.cm.fn.yo.container.norm";
import func_channel_cm_yu_container_atom from './channel.cm.fn.yu.container.atom';

import func_channel_macrocosm_yi_modulat from './channel.macrocosm.fn.yi.modulat';
import func_channel_macrocosm_yo_container from './channel.macrocosm.fn.yo.container';
import func_channel_macrocosm_yo_tpl from './channel.macrocosm.fn.yo.tpl';

import func_channel_form_yi_control from './channel.form.fn.yi.control';
import func_channel_form_yo_control from './channel.form.fn.yo.control';

import func_channel_grid_yi_list_configuration from './channel.grid.fn.yi.list.configuration';
import func_channel_grid_yi_list_container from './channel.grid.fn.yi.list.container';
import func_channel_grid_yi_list_op from './channel.grid.fn.yi.list.op';

import func_channel_grid_yo_list from './channel.grid.fn.yo.list';
import func_channel_grid_yo_list_segment from './channel.grid.fn.yo.list.segment';
import func_channel_grid_yo_qr_qbe from './channel.grid.fn.yo.qr.qbe';

// Yi
import yiStandard from './channel.@fn.yi.standard';
import yiAssist from './channel.@fn.yi.assist';
// Xui
import xuiContainer from './aero.@fn.xui.container';
import func_aero_xui_component from './aero.fn.xui.component';

export default {
    xuiContainer,
    // xuiGrid
    // xuiDecorator
    ...func_aero_xui_component,
    // ========================================= Yi
    yiAssist,
    yiStandard,
    // yiContainer
    // yiControl
    ...func_channel_cm_yi_container_atom,
    // yiCombine
    // yiModule
    ...func_channel_macrocosm_yi_modulat,
    // yiFormPart
    ...func_channel_form_yi_control,
    // yiListQuery
    // yiListOptions
    // yiListSynonym
    // yiListPlugin
    // yiListLazy
    // yiColumn
    ...func_channel_grid_yi_list_configuration,
    // yiListTable
    // yiListView
    // yiListTab
    ...func_channel_grid_yi_list_container,
    // yiListOp
    ...func_channel_grid_yi_list_op,
    // ========================================= Yo
    yoAmbient,
    yoDynamic,
    yoRender,
    // yoAction
    // yoExtension
    ...func_channel_event_yo_action,
    // yoAtomContainer
    // yoAtomComponent
    ...func_channel_cm_yo_container_atom,
    // yoTabPage
    // yoDialog
    ...func_channel_macrocosm_yo_container,
    // yoContainer
    // yoComponent
    // yoControl
    ...func_channel_cm_yo_container_norm,
    // yoFilter
    // yoForm
    // yoFormAdd
    // yoFormEdit
    ...func_channel_form_yo_control,
    // yoPolymorphism
    // yoGrid
    // yoList
    // yoTable
    ...func_channel_grid_yo_list,
    // yoListBatch
    // yoListOpen
    // yoListGrid
    // yoListSearch
    // yoListExtra
    ...func_channel_grid_yo_list_segment,
    // yoQrCond
    // yoQrTag
    // yoQrQBE
    ...func_channel_grid_yo_qr_qbe,
    // yoTplSider
    // yoTplAccount
    // yoTplHeader
    // yoTplNavigation
    ...func_channel_macrocosm_yo_tpl,
    // ========================================= Yu
    // yuRouter
    // yuContainer
    ...func_channel_cm_yu_container_atom
}