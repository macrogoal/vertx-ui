// import __RS from './idyl.fn.rs.state';
import Ux from 'ux';

export default {
    // rxLoading,
    // rxDirty,
    // rxVisible,
    rxClose: (reference) => (data = {}, addOn = {}) => {
        const updated = {};
        updated.$inited = data;
        Object.assign(updated, addOn);
        return Ux.of(reference).in(updated).hide().future(() => Ux.promise(updated));
    }
}