import func_idyl_rs_state from './idyl.fn.rs.state';
import func_idyl_rx_container_batch from './idyl.fn.rx.container.batch';
import func_idyl_rx_container_tab from './idyl.fn.rx.container.tab';
import func_idyl_rx_qr_criteria from './idyl.fn.rx.ir.criteria';
import func_idyl_rx_qr_qbe from './idyl.fn.rx.qr.qbe';
import func_idyl_rx_qr_view from './idyl.fn.rx.qr.view';
import func_idyl_rx_row_action from './idyl.fn.rx.row.action';
import func_idyl_rx_source from './idyl.fn.rx.source';
import func_idyl_rx_state from './idyl.fn.rx.state';

import func_habit_config_control from './habit.fn.config.control';
import func_habit_config_event from './habit.fn.config.event';
import func_habit_up_pre_condition from './habit.fn.up.pre.condition';
import func_habit_parser_action from './habit.fn.parser.action';

import func_lkway_on_application from './lkway.fn.on.application';
import func_lkway_on_relation_ship from './lkway.fn.on.relation.ship';

import func_kind_a4_menu_item from './kind.fn.a4.menu.item';

import func_rapid_sex_ex_action from './rapid.fn.sex.ex.action';

import func_levy_in_economy_remote from './levy.fn.in.economy.remote';

import func_pedestal_map_pipeline from './pedestal.fn.map.pipeline';
import func_pedestal_to_atom from './pedestal.fn.to.atom';
import func_pedestal_to_web_control from './pedestal.fn.to.web.control';

import func_tracer_parser_logging from './tracer.fn.parser.logging';

import v_pedestal_constant_option from './pedestal.v.constant.option';
// yo / yi / yu
import index_entry_channel from './channel.zero.index';

export default {
    // ant 4
    ...func_kind_a4_menu_item,

    // in api for ajax remoting
    ...func_levy_in_economy_remote,

    // rx function executors
    ...func_idyl_rs_state,
    ...func_idyl_rx_container_batch,
    ...func_idyl_rx_container_tab,
    ...func_idyl_rx_qr_criteria,
    ...func_idyl_rx_qr_qbe,
    ...func_idyl_rx_qr_view,
    ...func_idyl_rx_row_action,
    ...func_idyl_rx_source,
    ...func_idyl_rx_state,

    ...func_habit_config_control,
    ...func_habit_config_event,
    ...func_habit_parser_action,
    ...func_habit_up_pre_condition,

    ...func_lkway_on_application,
    ...func_lkway_on_relation_ship,

    ...func_rapid_sex_ex_action,

    ...func_pedestal_map_pipeline,
    ...func_pedestal_to_atom,
    ...func_pedestal_to_web_control,
    ...func_tracer_parser_logging,
    // index.entry.channel
    ...index_entry_channel,
    /*
     * {
     *     Opt:
     *     V:
     *     PLUGIN:
     *     Mode:
     * }
     * -- Opt / Mode 未遵循新命名规范，暂时维持现状
     * -- V / PLUGIN 暂定维持
     */
    ...v_pedestal_constant_option,
}