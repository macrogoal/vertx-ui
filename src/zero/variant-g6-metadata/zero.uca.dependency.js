import __Zn from '../zero.module.dependency';
import G6_LIBRARY from '../variant-g6-library';

export default {
    ...__Zn,
    G6_LIBRARY,
}