import func_rapid_sex_batch from './rapid.fn.sex.batch';
import func_rapid_sex_callback from './rapid.fn.sex.callback';
import func_rapid_sex_configuration from './rapid.fn.sex.configuration';

import func_topology_g6_configuration from './topology.fn.g6.configuration';
import func_topology_g6_data from './topology.fn.g6.data';
import func_topology_g6_page_lifecycle from './topology.fn.g6.page.lifecycle';

import func_topology_x6_action from './topology.fn.x6.action';
import func_topology_x6_ui from './topology.fn.x6.ui';

import func_topology_x6x_rx_bind from './topology.fn.x6x.rx.bind';
import func_topology_x6o_on_bind from './topology.fn.x6o.on.bind';

import func_zero_rx_etat_critical from './zero.fn.rx.etat.critical';

export default {
    ...func_rapid_sex_batch,
    ...func_rapid_sex_callback,
    ...func_rapid_sex_configuration,

    ...func_topology_g6_configuration,
    ...func_topology_g6_data,
    ...func_topology_g6_page_lifecycle,

    ...func_topology_x6_action,
    ...func_topology_x6_ui,

    ...func_topology_x6x_rx_bind,
    ...func_topology_x6o_on_bind,

    ...func_zero_rx_etat_critical,
}