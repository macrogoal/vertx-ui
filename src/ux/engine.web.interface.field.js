// 内部要用的接口函数

import __Zs from 'zs';

const __UCA = __Zs.V_UCA_CONTROL;
// =====================================================
// 按钮
// =====================================================
const aiAction = (reference, jsx = {}) => __UCA.aiAction(reference, jsx);
const aiSubmit = (reference, optionJsx = {}) => __UCA.aiSubmit(reference, optionJsx);

const aiButtonGroup = (reference, buttons = []) => __Zs.aiButtonGroup(reference, buttons);

const aiButton = (reference, button = {}) => __Zs.aiButton(reference, button);
// =====================================================
// 非操作的交互式组件
// =====================================================
// import Input from './O.input';
const aiInput = (reference, jsx = {}, onChange) => __UCA.aiInput(reference, jsx, onChange);

// import InputPassword from './O.input.password';
const aiPassword = (reference, jsx = {}, onChange) => __UCA.aiPassword(reference, jsx, onChange);

const aiCaptcha = (reference, jsx = {}, onChange) => __UCA.aiCaptcha(reference, jsx, onChange);
const aiProtocol = (reference, jsx = {}, onChange) => __UCA.aiProtocol(reference, jsx, onChange);

// import InputNumber from './O.input.number';
const aiInputNumber = (reference, jsx = {}, onChange) => __UCA.aiInputNumber(reference, jsx, onChange);


// import Hidden from './O.hidden';
const aiHidden = (reference, jsx = {}) => __UCA.aiHidden(reference, jsx);


// import Select from './O.select';
const aiSelect = (reference, input = {}, onChange) => __UCA.aiSelect(reference, input, onChange);


// import TextArea from './O.textarea';
const aiTextArea = (reference, jsx = {}) => __UCA.aiTextArea(reference, jsx);


// import DatePicker from './O.picker.date';
const aiDatePicker = (reference, jsx = {}, onChange) => __UCA.aiDatePicker(reference, jsx, onChange);


// import TimePicker from './O.picker.time';
const aiTimePicker = (reference, jsx = {}, onChange) => __UCA.aiTimePicker(reference, jsx, onChange);

// import Checkbox from './O.check.box';
const aiCheckbox = (reference, jsx = {}, onChange) => __UCA.aiCheckbox(reference, jsx, onChange);


// import TreeSelect from './O.tree.select';
const aiTreeSelect = (reference, jsx = {}, onChange) => __UCA.aiTreeSelect(reference, jsx, onChange);


// import Radio from './O.radio';
const aiRadio = (reference, jsx = {}, onChange) => __UCA.aiRadio(reference, jsx, onChange);


// import TreeSelector from './O.selector.tree';
const aiTreeSelector = (reference, jsx = {}) => __UCA.aiTreeSelector(reference, jsx);


// import TableEditor from './O.editor.table';
const aiTableEditor = (reference, jsx = {}, onChange) => __UCA.aiTableEditor(reference, jsx, onChange);


// import SearchRangeDate from './O.search.range.date';
const aiSearchRangeDate = (reference, jsx = {}) => __UCA.aiSearchRangeDate(reference, jsx);


// import SearchInput from './O.search.input';
const aiSearchInput = (reference, jsx = {}) => __UCA.aiSearchInput(reference, jsx);


//import MatrixSelector from './O.selector.matrix';
const aiMatrixSelector = (reference, jsx = {}) => __UCA.aiMatrixSelector(reference, jsx);


//import Magic from './O.magic';
const aiMagic = (reference, jsx = {}) => __UCA.aiMagic(reference, jsx);


//import ListSelector from './O.selector.list';
const aiListSelector = (reference, jsx = {}) => __UCA.aiListSelector(reference, jsx);

const aiUserSelector = (reference, jsx = {}) => __UCA.aiUserSelector(reference, jsx);

const aiUserLeader = (reference, jsx = {}) => __UCA.aiUserLeader(reference, jsx);

const aiGroupSwitcher = (reference, jsx = {}) => __UCA.aiGroupSwitcher(reference, jsx);

const aiUserGroup = (reference, jsx = {}) => __UCA.aiUserGroup(reference, jsx);

// import JsonEditor from './O.editor.json';
const aiJsonEditor = (reference, jsx = {}, onChange) => __UCA.aiJsonEditor(reference, jsx, onChange);


//import InputArray from './O.input.array';
const aiInputMulti = (reference, jsx = {}, onChange) => __UCA.aiInputMulti(reference, jsx, onChange);


// import FileUpload from './O.file.upload';
const aiFileUpload = (reference, jsx = {}, onChange) => __UCA.aiFileUpload(reference, jsx, onChange);

const aiFileLogo = (reference, jsx = {}, onChange) => __UCA.aiFileLogo(reference, jsx, onChange);

const aiFileBatch = (reference, jsx = {}, onChange) => __UCA.aiFileBatch(reference, jsx, onChange);


// import DialogEditor from './O.editor.dialog';
const aiDialogEditor = (reference, jsx = {}) => __UCA.aiDialogEditor(reference, jsx);


// import Transfer from './O.transfer';
const aiTransfer = (reference, jsx = {}, onChange) => __UCA.aiTransfer(reference, jsx, onChange);


// import CheckJson from './O.check.json';
const aiCheckJson = (reference, jsx = {}, onChange) => __UCA.aiCheckJson(reference, jsx, onChange);


// import AddressSelector from './O.selector.address';
const aiAddressSelector = (reference, jsx = {}, onChange) => __UCA.aiAddressSelector(reference, jsx, onChange);


// import RichEditor from './O.editor.rich';
const aiBraftEditor = (reference, jsx = {}, onChange) => __UCA.aiBraftEditor(reference, jsx, onChange);
// import aiTableTransfer from './O.tree.select';
const aiTableTransfer = (reference, jsx = {}, onChange) => __UCA.aiTableTransfer(reference, jsx, onChange);
const exported = {
    // 验证码
    aiCaptcha,
    ai2Captcha: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiCaptcha(reference, jsx, fnChange);
    },
    // 协议专用（多个解析按钮）
    aiProtocol,
    ai2Protocol: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiProtocol(reference, jsx, fnChange);
    },
    // 常规录入
    aiInput,
    ai2Input: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiInput(reference, jsx, fnChange);
    },

    // 密码输入框
    aiPassword,
    ai2Password: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiPassword(reference, jsx, fnChange);
    },


    // 数字输入框
    aiInputNumber,
    ai2InputNumber: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiInputNumber(reference, jsx, fnChange);
    },


    aiInputMulti,
    ai2InputMulti: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiInputMulti(reference, jsx, fnChange);
    },

    // 多选框
    aiCheckbox,
    ai2Checkbox: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiCheckbox(reference, jsx, fnChange);
    },

    aiCheckJson,

    aiSubmit,
    aiAction,

    // 隐藏元素
    aiHidden,


    // 显示数据
    aiMagic,


    // 下拉
    aiSelect,
    ai2Select: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiSelect(reference, jsx, fnChange);
    },


    // 多行文本输入
    aiTextArea,
    ai2TextArea: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiTextArea(reference, jsx, fnChange);
    },


    // 日期选择器
    aiDatePicker,
    ai2DatePicker: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiDatePicker(reference, jsx, fnChange);
    },


    // 时间选择
    aiTimePicker,


    // 树下拉选择器
    aiTreeSelect,
    ai2TreeSelect: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiTreeSelect(reference, jsx, fnChange);
    },


    // 单选框
    aiRadio,
    ai2Radio: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiRadio(reference, jsx, fnChange);
    },


    // 选择穿梭框
    aiTransfer,
    ai2Transfer: (onChange) => (reference, jsx = {}) => {
        const fnChange = onChange.apply(null, [reference]);
        return aiTransfer(reference, jsx, fnChange);
    },


    // 列表多选器
    aiMatrixSelector,
    ai2MatrixSelector: (mockData = {}) => (reference, jsx = {}) => {
        jsx.mock = mockData;
        return aiMatrixSelector(reference, jsx);
    },


    // 列表选择器
    aiListSelector,
    ai2ListSelector: (mockData = {}) => (reference, jsx = {}) => {
        jsx.mock = mockData;
        return aiListSelector(reference, jsx);
    },


    // 树选择器
    aiTreeSelector,
    ai2TreeSelector: (mockData = {}) => (reference, jsx = {}) => {
        jsx.mock = mockData;
        return aiTreeSelector(reference, jsx);
    },


    // 地址选择器
    aiAddressSelector,
    ai2AddressSelector: (onSelect, mockData = {}) => (reference, jsx = {}) => {
        const fnChange = onSelect.apply(null, [reference]);
        jsx.mock = mockData;
        return aiAddressSelector(reference, jsx, fnChange);
    },


    // 用户选择器
    aiUserSelector,
    // 用户组切换器,
    aiGroupSwitcher,
    // 经理选择器
    aiUserLeader,
    // 用户组设置
    aiUserGroup,

    // 上传组件
    aiFileUpload,


    // 图标上传
    aiFileLogo,


    // 批量上传组件
    aiFileBatch,


    // 表格编辑器
    aiTableEditor,


    // 富文本编辑器
    aiBraftEditor,


    aiButton,
    aiButtonGroup,


    // 弹出框选择器
    aiDialogEditor,


    // Json编辑器
    aiJsonEditor,


    // 搜索专用
    aiSearchInput,


    // 时间搜索专用
    aiSearchRangeDate,

    // 树 + 表格编辑穿梭
    aiTableTransfer,
};
export default exported;