import __Zo from 'zo';

/**
 * ##「标准」`Ux.token`
 *
 * 返回安全请求中的令牌信息，最终会根据应用模式处理成 Authorization 的计算值。
 *
 * @memberOf module:_primary
 * @return {String} 返回当前用户登录过后的令牌`token`信息。
 */
const token = () => __Zo.token();
/**
 * ##「标准」`Ux.signature`
 *
 * Zero UI中的RESTful 数字签名功能专用函数。
 *
 * @memberOf module:_primary
 * @param {String} uri 待签名的路径信息。
 * @param {String} method 待签名的方法信息。
 * @param {Object} params 待签名的方法参数。
 */
const signature = (uri, method = "GET", params = {}) => __Zo.signature(uri, method, params);

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    // 签名专用函数
    signature,
    // 读取当前用户登录过后的token，如果未登录则会返回undefined
    token,
    /*
     * 返回值在非动态处理时新增 options 的限制性操作
     */
    aclOp: (options = {}, ops) => __Zo.aclOp(options, ops),
    aclData: ($inited = {}, reference, $edition) => __Zo.aclData($inited, reference, $edition),
    aclSubmit: (params = {}, reference) => __Zo.aclSubmit(params, reference)
}