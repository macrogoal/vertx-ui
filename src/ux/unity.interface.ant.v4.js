import __Zo from 'zo';
import __Zs from 'zs';
import __Zn from "zone";

const v4Icon = (type, rest = {}) =>
    __Zo.v4Icon(type, rest);

const v4FormFailure = (target, id, show) =>
    __Zo.v4FormFailure(target, id, show);

const v4FormItem = (item = {}, cell = {}) =>
    __Zo.v4FormItem(item, cell);

const v4ChildItem = (items = [], reference) =>
    __Zs.v4ChildItem(items, reference);

const v4Items = (items = [], configuration = {}, reference) =>
    __Zs.v4Items(items, configuration, reference);


export default {
    // Icon connected to ant-design V3
    v4Icon,
    // Form Error connected to ant-design V4
    v4FormFailure,
    v4FormItem,
    // v4Child / v4ChildItem
    v4Child: __Zs.v4Child,
    v4ChildItem,
    // v4Items / v4Tree
    v4Items,
    // Modal / message / notification
    v4Modal: __Zn.v4Modal,
    v4Notify: __Zn.v4Notify,
    v4Message: __Zn.v4Message,
    // 组件问题
    V4App: __Zn.V4App,
    V4InputGroup: __Zn.V4InputGroup,
}