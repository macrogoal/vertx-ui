import __Zs from 'zs';
import __Zi from 'zi';

const aiTree = (item = {}, rest = {}) => __Zs.aiTree(item, rest);
const aiField = (reference, values = {}, raft = {}) =>
    __Zs.aiField(reference, values, raft);
const aiInit = (reference, values) =>
    __Zi.xtInited(reference, values);
/*
 * config 数据结构：
 * {
 *     formKey: 表单的 key（防止修改过后的不按预期的变更）
 * }
 */
const aiForm = (reference, values, configuration = {}) =>
    __Zs.aiForm(reference, values, configuration);
const aiFormInput = (reference, values, raft = {}) =>
    __Zs.aiFormInput(reference, values, raft);
const aiFormField = (reference, fieldConfig = {}, fnJsx) =>
    __Zs.aiFormField(reference, fieldConfig, fnJsx);
// ---------------- O.tab.js
const aiTabPage = (reference, children = {}) => __Zs.aiTabPage(reference, children);
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    aiTabPage,
    aiTree,
    aiForm,
    aiInit, // 统一处理
    aiField,
    aiFormInput,
    aiFormField,
}