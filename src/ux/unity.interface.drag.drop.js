import __Zs from 'zs';

export default {
    /*
     * 用于绑定 targetSpec 中的 hover 方法
     * 直接设置 $hover
     */
    dndDropColor: (reference, isOver, additional) =>
        __Zs.dndDropColor(reference, isOver, additional),

    dndDropWrap: (reference, renderFn) => __Zs.dndDropWrap(reference, renderFn),
    dndDropHoc: (options) => __Zs.dndDropHoc(options),

    dndDragHoc: (options) => __Zs.dndDragHoc(options),
    dndDragWrap: (reference, renderFn) => __Zs.dndDragWrap(reference, renderFn)
}