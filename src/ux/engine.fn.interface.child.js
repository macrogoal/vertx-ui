import __Zs from 'zs';

const aiChildItem = (items = [], reference, Component) =>
    __Zs.aiChildItem(items, reference, Component);

export default {
    aiChildItem
}