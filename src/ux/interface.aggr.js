import __ZS from 'zs';

export default {
    /*
     * Sum聚集
     *
     * Result:
     * name = value
     * name = value
     */
    aggrSum: (grouped = {}, config = {}, source = []) =>
        __ZS.aggrSum(grouped, config, source)
}