import __Zs from 'zs';

const aiAddRemove = (reference, config = {}) =>
    __Zs.aiAddRemove(reference, config);
const aiTopBar = (reference, buttons = [], disabled = {}) =>
    __Zs.aiTopBar(reference, buttons, disabled);
export default {
    aiAddRemove,        // Remove / Add Button
    aiCommand: __Zs.aiCommand,          // 代替原来的 opCmdPopover
    aiTopBar,           // 左上专用主按钮，右上为 aiExtra
}