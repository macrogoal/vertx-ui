import __Zo from 'zo';

/**
 * 验证规则属性
 * message：校验文件
 * type: 可选（内建类型）
 * required：是否必填
 * len:字段长度
 * min:最小长度
 * max:最大长度
 * enum: 枚举类型
 * pattern:正则表达式校验
 * transform:校验前转换字段值
 * validator: 自定义校验
 * @method _uiDisplay
 * @private
 * @param row 显示行数据
 * @param addition 额外风格
 * @param config
 * **/
const applyRow = (row = {}, addition = {}, config) => __Zo.applyRow(row, addition, config);
const applyView = (config = {}, field) => __Zo.applyView(config, field);
const applyRender = (renders = {}, code) => __Zo.applyRender(renders, code);
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    // 读取表单默认值
    applyRow,
    applyView,
    applyRender,
};