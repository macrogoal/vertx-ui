import __Zs from 'zs';


const uiTitle = (reference, fnJsx, configKey = "page") =>
    __Zs.uiTitle(reference, fnJsx, configKey);
const uiButton = (actions, executor = {}, reference) =>
    __Zs.uiButton(actions, executor, reference);
const uiModal = (config = {}, fnChild, reference) =>
    __Zs.uiModal(config, fnChild, reference);
const uiQrSearch = (search = {}, reference, rxQuery) =>
    __Zs.uiQrSearch(search, reference, rxQuery);
const uiQrRange = (range = {}, reference, rxQuery) =>
    __Zs.uiQrRange(range, reference, rxQuery);
/*
 * 快速开发专用
 * - ui：返回 jsx
 * - uiQr：搜索专用组件快速开发
 */
export default {
    // 「搜索」UI系列方法
    uiQrSearch,
    uiQrRange,

    // ui系列方法（快速处理）
    uiButton,
    uiModal,
    uiTitle,
}