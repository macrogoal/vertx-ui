import __Zs from 'zs';

const dataIo = (request = {}, config = {}, reference) =>
    __Zs.dataIo(request, config, reference);
export default {
    dataIo,
    /*
     * field = {
            "inSource": "linkageAsset",
            "inPath": "targetData",
            "outType": "ARRAY"
     * }
     */
    dataWrite: (request = {}, io = {}, reference) =>
        __Zs.dataWrite(request, io, reference),
    dataRead: ($inited = {}, config = {}, reference) =>
        __Zs.dataRead($inited, config, reference),
}