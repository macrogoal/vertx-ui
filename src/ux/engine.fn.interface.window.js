import __Zi from 'zi';

/**
 * @deprecated
 */
const ltrResize = (reference) => __Zi.wl(reference);

export default {
    ltrResize,
    wl: ltrResize
}