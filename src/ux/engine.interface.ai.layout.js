// 导入当前目录
// 导入外层

import __Zi from 'zi';

const aiLayout = (item, layout = {}) => __Zi.aiLayout(item, layout);

const aiLayoutItem = (window = 1, key) => __Zi.aiLayoutItem(window, key);

const aiLayoutAdjust = (window = 1) => __Zi.aiLayoutAdjust(window);

export default {
    aiLayout,
    aiLayoutItem,
    aiLayoutAdjust, aiAdjust: aiLayoutAdjust,
};