import __Zi from 'zi';

/**
 * ## 「标准」`Ux.qrForm`
 *
 * 针对查询专用表单的条件数据收集处理，构造查询$filters变量专用，收集表单中的条件数据构造查询条件信息，
 *
 * 1. connector有两个值：`AND | OR`，用于设置条件和条件之间的条件连接符。
 * 2. 如果条件中遇到值：`__DELETE__`的值，则将该条件删除掉。
 * 3. 系统会搜索同名属性，和`op`组合成最新条件，完成搜索表单的提交操作。
 *
 * > 最终生成的查询条件可在日志中查看。
 *
 * @memberOf module:_qr
 * @method qrForm
 * @param {Object} input 输入的字段信息。
 * @param {String} connector 查询条件。
 * @param {Object|ReactComponent} reference React对应组件引用。
 * @returns {Object} 返回最终查询条件，写入$filters变量。
 */
const qrForm = (input, connector = "AND", reference) =>
    __Zi.qrForm(input, connector, reference);
const qrNorm = (condition = {}, configuration = {}) =>
    __Zi.qrNorm(condition, configuration);
/**
 * ## 「标准」`Ux.qrTerms`
 *
 * 收集列中的`$filter`过滤配置数据，生成最终的 $terms 变量。
 *
 * @memberOf module:_qr
 * @param {Array} columns 列过滤中的所有列配置数据
 * @returns {Object} 返回列配置构造的数据
 */
const qrTerms = (columns = []) =>
    __Zi.qrTerms(columns);


/**
 * ## 「标准」`Ux.qrInput`
 *
 * 单独输入框的搜索条件构造专用函数，如果清除则值设为`__DELETE__`。
 *
 * @memberOf module:_qr
 * @param {Array} cond 查询条件字段信息。
 * @param {any} value 值信息，如果无值则清除条件。
 */
const qrInput = (cond = [], value) =>
    __Zi.qrInput(cond, value);
/**
 *
 * ## 「标准」`Ux.qrCombine`
 *
 * 查询引擎专用的合并函数，深度计算，内部使用了`QQuery`对象。
 *
 * 1. 以`query`参数为基础查询引擎数据，构造模式`(query,reference)`。
 * 2. 两个参数构造`QQuery`对象，然后将condition用AND方式追加到查询条件中。
 *
 * > 注，最终条件会移除掉`__DELETE__`值的条件。
 *
 * @memberOf module:_qr
 * @method qrCombine
 * @param {Object} query 查询条件。
 * @param {Object|ReactComponent} reference React对应组件引用。
 * @param {String[]} condition 查询条件处理。
 * @returns {any} 返回最终的 query 结构。
 */
const qrCombine = (query = {}, reference, ...condition) =>
    __Zi.qrCombine.apply(this, [query, reference].concat(condition));

/**
 * ## 「标准」`Ux.qrCommon`
 *
 * 复杂内容核心操作，用于设置默认的 $query 信息
 *
 * ### 优先级选取
 *
 * 1. props 中的 $query 优先
 * 2. config 中的 query 第一级，直接合并 config.query （全合并）
 * 3. config 中的 ajax.magic 合并（需解析，只合并 criteria）
 *
 *
 * @memberOf module:_qr
 * @method qrCommon
 * @param {Object|ReactComponent} reference React对应组件引用。
 * @param {Object} config 查询配置
 * @returns {Object} 返回最终的 query 结构。
 */
const qrCommon = (reference, config) =>
    __Zi.qrCommon(reference, config);

/**
 * ## 「标准」`Ux.qrComplex`
 *
 * 复杂模式的处理流程，三合一的查询条件处理，处理不同情况的查询条件，执行合并。
 *
 * 1. `$condition`：当前环境的基础条件，该基础条件受`列过滤`的影响，触发了列过滤后该变量会受到影响。
 * 2. `$terms`：该变量是计算基础，保存了列中定义了`filter`的列配置，换算过后的定义结果会保存在 $terms 变量中。
 * 3. `$filters`：该变量保存的是高级搜索表单存储的最终查询条件。
 *
 * 需要说明查询条件来自于几个源头：
 *
 * 1. props属性中的$query条件，同时包含config配置中的条件，最终计算存储在state中，生成$query新变量（状态中）。
 * 2. 如果是列过滤功能，则直接修改$condition变量的值。
 * 3. 如果是基础搜索和高级搜索，则直接修改$filters变量的值。
 *
 * > 所以最终查询条件的计算是：reference.state中的`$query + $condition + $filters`三者合一，借用`QQuery`实现查询条件的复杂运算。
 *
 * @memberOf module:_qr
 * @method qrComplex
 * @param {Object} query 查询条件专用结构。
 * @param {Object|ReactComponent} reference React对应组件引用。
 * @returns {Object} 返回最终的 query 结构。
 */
const qrComplex = (query = {}, reference) =>
    __Zi.qrComplex(query, reference);

/**
 * ## 「标准」`Ux.qrInherit`
 *
 * 计算Qr需要的继承变量，用于继承查询条件专用。
 *
 * 1. 先从props中读取$query变量。
 * 2. 如果未传递props属性中的$query，则从state中读取：`state.query`信息（ListComplex中使用）
 *      * ExListComplex
 *      * ExListQuery
 *      * ExListOpen
 *
 * 以上三个组件为目前使用query继承的组件。
 *
 * @memberOf module:_qr
 * @method qrInherit
 * @param {Object|ReactComponent} reference React对应组件引用。
 * @returns {Object} 返回最终的 query 结构。
 */
const qrInherit = (reference) =>
    __Zi.qrInherit(reference);
const qrMessage = (data = {}, config = {}, form = {}) =>
    __Zi.qrMessage(data, config, form);
// ------------------ New Api for Qr Engine ( Support Updating )
const qrNil = (condition) => __Zi.qrNil(condition);
const qrOne = (condition) => __Zi.qrOne(condition);
const qrOp = (condition) => __Zi.qrOp(condition);
// reference will provide the parameters for syntax parsing here.
const qrAndH = (reference = {}, criteria, field, value) =>
    __Zi.qrAndH(reference, criteria, field, value);

const qrAndQH = (reference, query, field, value) =>
    __Zi.qrAndQH(reference, query, field, value);
export default {
    qrNil,
    qrOne,
    qrOp,
    /*
     * 对应后端
     * Ux.irAndH
     * Ux.irAndQH
     */
    qrAndH,
    qrAndQH,
    /*
     * 三层组件：
     * -- Component Parent -> $condition
     * -- Component -> Search $condition
     * -- Component -> $query ( 默认条件 )
     */

    qrComplex,
    qrCombine,
    qrCommon,
    qrInherit,
    /*
     * 列筛选中配置了 $filter，需要根据 $filter 生成配置
     * 1）$filter 的类型信息
     * 2）$filter 列的数据类型（后端查询需要使用）
     */
    qrTerms,
    /*
     * 清除当前 $condition 的条件
     * 清除当前 $searchText
     */
    // qrClear,
    /*
     * 提取参数专用方法
     * 1）qrInput：单字段多值 Or 连接（主要针对单独输入框）
     * 2）qrForm：完整表单（针对Form提交的核心数据）
     */
    qrInput,
    qrForm,
    qrNorm,
    qrMessage,
    // Internal Qr API
    irData: (reference) => __Zi.irData(reference),
    irSubmit: (reference) => __Zi.irSubmit(reference),
}